#include<iostream>


#include "opencv2/core/core_c.h"
#include "opencv2/core/core.hpp"
#include "opencv2/flann/miniflann.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/video.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/ml/ml.hpp"
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/contrib/contrib.hpp"

using namespace std;
using namespace cv;

char* img_1,*img_2,*img_out;

void ParseArguments(int argc, char** argv)
{
    img_1=argv[1];
    img_2=argv[2];
    img_out=argv[3];
}

void MyStereoBM()
{
    int pre_filter_cap = 31;
    int pre_filter_size=41;
    int sad_window_size = 9;
    int num_disparities = 128;
    int texture_threshold=20;
    int uniqueness_ratio = 10;
    StereoBM myStereo(0,num_disparities,sad_window_size);
    myStereo.state->preFilterCap=pre_filter_cap;
    myStereo.state->preFilterSize=pre_filter_size;
    myStereo.state->textureThreshold=texture_threshold;
    myStereo.state->uniquenessRatio=uniqueness_ratio;
    //myStereo.pre_filter_cap=31;

    Mat img1=imread(img_1,0), img2=imread(img_2,0), disp;
    myStereo(img1,img2,disp);
    Mat res(disp.rows,disp.cols,CV_16UC1);

    short max_value=0,min_value=10000;
    for(int h=0;h<disp.rows;h++)
    {
        for(int w=0;w<disp.cols;w++)
        {
            if(max_value<disp.at<short>(h,w))
                max_value=disp.at<short>(h,w);
            if(min_value>disp.at<short>(h,w))
                min_value=disp.at<short>(h,w);
            res.at<unsigned short>(h,w)=(unsigned short) max(255*disp.at<short>(h,w)/16+0.5,0.0);
        }
    }
    cout<<max_value<<" "<<min_value<<endl;
    imwrite(img_out,res);
}

void Stereo()
{
    int pre_filter_cap = 63;
    int sad_window_size = 3;
    int p1 = sad_window_size*sad_window_size*4;
    int p2 = sad_window_size*sad_window_size*32;
    int min_disparity = 0;
    int num_disparities = 128;
    int uniqueness_ratio = 10;
    int speckle_window_size = 100;
    int speckle_range = 32;
    int disp_max_diff = 1;
    bool full_dp = 1;
    StereoSGBM myStereo(min_disparity,num_disparities,
    sad_window_size,p1,p2,disp_max_diff,pre_filter_cap,
    uniqueness_ratio,speckle_window_size,speckle_range,full_dp);
    Mat img1=imread(img_1), img2=imread(img_2), disp;
    myStereo(img1,img2,disp);
    Mat res(disp.rows,disp.cols,CV_16UC1);
    //cout<<disp.type()<<endl;
    //cout<<CV_16S<<endl;
    short max_value=0,min_value=10000;
    for(int h=0;h<disp.rows;h++)
    {
        for(int w=0;w<disp.cols;w++)
        {
            if(max_value<disp.at<short>(h,w))
                max_value=disp.at<short>(h,w);
            if(min_value>disp.at<short>(h,w))
                min_value=disp.at<short>(h,w);
            res.at<unsigned short>(h,w)=(unsigned short) max(255*disp.at<short>(h,w)/16+0.5,0.0);
        }
    }

    cout<<max_value<<" "<<min_value<<endl;
    //imwrite("test.png",disp);
    imwrite(img_out,res);
}

int main(int argc, char** argv)
{
    cout<<(1<<4)<<endl;
    ParseArguments(argc,argv);
    MyStereoBM();
}
